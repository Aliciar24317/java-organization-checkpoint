package com.galvanize;

public class Booking {


    public enum Type{
        Conference, Suite, Auditorium, Classroom
    }
    private static Type type;
    private static String roomNumber = "";
    private static String startTime;
    private static String endTime;

    public Booking(Booking firstInput) {
        type = firstInput.getType();
        roomNumber = firstInput.getRoomNumber();
        startTime = firstInput.getStartTime();
        endTime = firstInput.getEndTime();
    }

    public Booking(Type room, String roomNumber, String startTime, String endTime)
    {
        this.type = room;
        this.roomNumber = roomNumber;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Booking parse(String code)
    {
        String[] parser = code.split("-");
        if (parser[0].startsWith("a"))
        {
            setType(Type.Auditorium);
        } else if (parser[0].startsWith("s")) {
            setType(Type.Suite);
        }else if (parser[0].startsWith("r")) {
            setType(Type.Conference);
        }else
            setType(Type.Classroom);
        for(int i =1; i< parser[0].length(); i++) {
            roomNumber += parser[0].charAt(i);
        }
        startTime = parser[1];
        endTime = parser[2];

        Booking result = new Booking(type, roomNumber, startTime, endTime);
        return result;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }




    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }



    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }



    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


}
