package com.galvanize.formatters;

import com.galvanize.Booking;

public class CSVFormatter implements Formatter{
    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    private String output = "";

    public CSVFormatter(Booking firstInput) {
        format(firstInput);

    }

    @Override
    public String format(Booking input)
    {

        Booking attempt = new Booking(input.getType(), input.getRoomNumber(), input.getStartTime(), input.getEndTime());
        output = "type,room number,start time,end time\n" + attempt.getType() + "," + attempt.getRoomNumber() + "," + attempt.getStartTime() + ","
                + attempt.getEndTime();
//        System.out.println(attempt.getType() + ", " + attempt.getRoomNumber() + ", " + attempt.getStartTime() + ", "
//        + attempt.getEndTime());
        return output;
    }
}
